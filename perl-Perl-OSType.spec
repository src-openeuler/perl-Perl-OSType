%global perl_package_name Perl-OSType
Name:		perl-%{perl_package_name}
Version:	1.010
Release:	423
Summary:	Map Perl operating system names to generic types
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/%{perl_package_name}
BuildArch:	noarch
Source0:	https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/%{perl_package_name}-%{version}.tar.gz

BuildRequires:	gcc make perl-interpreter perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker) perl(Test::More)

%description
Modules that provide OS-specific behaviors often need to know if the current
operating system matches a more generic type of operating systems. For example,
'linux' is a type of 'Unix' operating system and so is 'freebsd'.

%package_help

%prep
%autosetup -n %{perl_package_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} -c $RPM_BUILD_ROOT

%check
make test

%files
%doc Changes CONTRIBUTING.mkdn README
%license LICENSE
%{perl_vendorlib}/Perl/

%files help
%{_mandir}/man3/Perl::OSType.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.010-423
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 hongjinghao <hongjinghao@huawei.com> - 1.010-422
- use perl_package_name

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.010-421
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete redundant file

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.010-420
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.010-419
- Package init
